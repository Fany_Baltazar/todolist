//Declara e inicializa array con encabezado
var todo = ['TO DO LIST'];

function Read()
{
  //hace el llamado a la librería
  const readline = require('readline');

  //Inicializa la interfaz con el command prompt
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: 'Indique el dato a agregar: '
  });

  //Pone nueva línea en el command prompt (salto de línea)
  rl.prompt();

  //pausa el input para recibir los valores
  rl.on('line', (dato) => {
    //con arrow funtion:
    //Agrega el dato al array
    todo.push(dato)

    //recorre el array y muestra lo que se tiene en ToDo.
    for (let value of todo)
    {
      console.log ("-", value)
    }

    rl.prompt();

    //cierra la lectura y envía mensaje de salida
    }).on('close', () => {
      console.log('Fin de ToDo List');
      //termina la ejecución
      process.exit(1);
    });
}

//Llamada a la función
Read()
